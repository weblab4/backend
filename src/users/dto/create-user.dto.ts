import { IsEmail, IsNotEmpty, Length } from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(10, 20)
  password: string;
  @IsNotEmpty()
  fullName: string;
  //@IsArray()
  //roles: ('admin' | 'user')[];
  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
